package car.sorter.app;

import java.util.List;
/**
 * Created by adi on 17.10.2014.
 */
public class OutputDevice {

	public void printf(List<Vehicle> result) {
		
		for(Vehicle vehicle : result){
			System.out.println("Vehicle: " + vehicle.getType() + " " + "Speed: " + vehicle.getSpeed() + "\n");
		}
	}
}
