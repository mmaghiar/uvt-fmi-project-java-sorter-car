package car.sorter.app;

import car.sorter.entities.IEntityWithSpeed;

/**
 * Created by adi on 17.10.2014.
 */
public class Vehicle implements IEntityWithSpeed{

	private double speed;
	private String type;

	public Vehicle(double speed, String type) {
		this.speed = speed;
		this.type = type;
	}

	public double getSpeed() {
		return this.speed;
	}
	
	public String getType(){
		return this.type;
	}

}
