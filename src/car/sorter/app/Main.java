package car.sorter.app;

import car.sorter.app.controller.AppController;

/**
 * Created by Maghiar Mihai Adrian on 17.10.2014.
 */
public class Main {
    public static void main(String[] args) {
    	
    	InputDevice iD = new InputDevice();
    	OutputDevice oD = new OutputDevice();
    	
    	AppController p = new AppController(iD, oD);
    	p.perform();

    }
}
