package car.sorter.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maghiar Mihai Adrian on 17.10.2014.
 */

public class InputDevice {
	
	public List<Vehicle> getVehicle(){

	    List<Vehicle> vehicles = new ArrayList<Vehicle>();
	    
	    vehicles.add(new Vehicle(20,"Tramway"));
	    vehicles.add(new Vehicle(54,"Bus"));
	    vehicles.add(new Vehicle(87,"Car"));
	    vehicles.add(new Vehicle(54,"Bus"));
	    vehicles.add(new Vehicle(33,"Bus"));
	    vehicles.add(new Vehicle(54,"Bus"));
	    vehicles.add(new Vehicle(10,"Tramway"));
	    vehicles.add(new Vehicle(15,"Tramway"));
	    
	    return vehicles;
	}
	
}
