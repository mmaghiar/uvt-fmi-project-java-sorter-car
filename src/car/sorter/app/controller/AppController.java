package car.sorter.app.controller;

import java.util.List;

import car.sorter.app.InputDevice;
import car.sorter.app.OutputDevice;
import car.sorter.app.Vehicle;
import car.sorter.comparator.SpeedComparator;
import car.sorter.sorter.BubbleSorter;
import car.sorter.sorter.ISorter;
import car.sorter.sorter.Sorter;

/**
 * Created by adi on 17.10.2014.
 */
public class AppController {
	
	private InputDevice inputDevice;
	private OutputDevice outputDevice;

	public AppController(InputDevice id, OutputDevice od){
		this.inputDevice = id;
		this.outputDevice = od;
	}
	
    public void perform(){
    	
        List<Vehicle> vehicles = inputDevice.getVehicle();
        
        ISorter<Vehicle> sorter  = new BubbleSorter<Vehicle>();
        List<Vehicle> result = sorter.sort(vehicles, new SpeedComparator<Vehicle>());
        
        outputDevice.printf(result);
        
    }


}
