package car.sorter.comparator;

import car.sorter.entities.IEntityWithSpeed;

public class SpeedComparator<T extends IEntityWithSpeed> implements IComparator<T>{

	double epsilon = 0.0001;
	
	public int compare(T s1, T s2) {
		if(s1.getSpeed() > s2.getSpeed()){
			return 1;
		} else if (Math.abs(s1.getSpeed() - s2.getSpeed()) < epsilon){
			return 0;
		} else {
			return -1;
		}
		
	}
	
}

