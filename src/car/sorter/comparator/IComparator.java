package car.sorter.comparator;

public interface IComparator <Vehicle>{

	public int compare(Vehicle s1, Vehicle s2);
	
}
