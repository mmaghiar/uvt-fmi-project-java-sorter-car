package car.sorter.sorter;

import java.util.List;

import car.sorter.comparator.IComparator;
import car.sorter.entities.IEntityWithSpeed;

public class Sorter<T extends IEntityWithSpeed> implements ISorter <T> {

	public List<T> sort(List<T> toSort, IComparator<T> comparator) {
		
		BubbleSorter<T> bs = new BubbleSorter<T>();
		
		return bs.sort(toSort, comparator);
	}

	

}
