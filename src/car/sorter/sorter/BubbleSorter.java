package car.sorter.sorter;

import java.util.List;

import car.sorter.comparator.IComparator;

/**
 * Created by adi on 17.10.2014.
 */

public class BubbleSorter<T>  implements ISorter <T> {
	
	
	public List<T> sort(List<T> list, IComparator<T> comp){
		
		T temp;
        for(int i=list.size()-1; i > 0; i--){
 
            for(int j=0; j < i; j++){
            	
                if(comp.compare(list.get(j), list.get(j+1)) == 1){
                	
                    temp=list.get(j);
                    list.set(j, list.get(j+1));
                    list.set(j+1, temp);
                    
                }
                
            }
        }
        return list;
	}
	
}
