package car.sorter.sorter;

import java.util.List;

import car.sorter.comparator.IComparator;

public interface ISorter <T>{
	public List<T> sort(List<T> list, IComparator<T> comparator);
}	
