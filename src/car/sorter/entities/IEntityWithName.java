package car.sorter.entities;

public interface IEntityWithName {
	public String getName();
}
