package car.sorter.entities;

public interface IEntityWithSpeed {
	public double getSpeed();
}
